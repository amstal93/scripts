#!/bin/bash

[ ! -z "$1" ] || { echo "Provide version/tag to run (e.g. v1.5.2, latest)" ; exit 1 ; }

docker kill gitlab-runner
docker rm gitlab-runner

docker run -d --name gitlab-runner --restart always \
           -v /var/run/docker.sock:/var/run/docker.sock \
           -v /srv/gitlab-runner/config:/etc/gitlab-runner \
           -v $HOME/.docker/config.json:/root/.docker/config.json \
              gitlab/gitlab-runner:$1
